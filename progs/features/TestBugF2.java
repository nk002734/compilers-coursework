class TestBugF1 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {
    
    boolean result;

    public boolean print(){
        System.out.println("Short circuit failed");
        result = true;
        return true;
    }

    public boolean f() {
        result = false;
        boolean x;
        x = true;
        if (x || print()){}
        return result;
    }
}