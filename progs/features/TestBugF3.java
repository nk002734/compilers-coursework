class TestBugF1 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {
    
    public boolean f() {
        boolean x;
        boolean y;
        boolean result;
        x = false;
        y = true;
        result = x || y;
        return result;
    }
}