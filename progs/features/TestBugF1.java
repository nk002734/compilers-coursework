class TestBugF1 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

class Test {
    
    public boolean f() {
        int x;
        int y;
        boolean result;
        x = 1;
        y = 0;
        result = x || y;
        return result;
    }
}